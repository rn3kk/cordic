#include <QCoreApplication>
#include <QtMath>
#include <QDebug>
#include <QFile>

int sign(double z)
{
  return (z>0?1:-1);
}

void sineValue(double x0, double y0, double angleShift, int maxStepCount, double& xResult, double& yResult)
{
  double x=x0, y=y0, angle=angleShift;

  for(int i=0; i< maxStepCount; ++i)
  {
    double iPow = qPow(2, -i);
    double x_r = x - sign(angle) * y * ( iPow );
    double y_r = y + sign(angle) * x * ( iPow );
    double angle_r = angle - sign(angle)*atan( iPow );

    x = x_r;
    y = y_r;
    angle = angle_r;
  }
  xResult=x * 0.607;
  yResult=y * 0.607;
}

int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);  

  double x0 = 1;
  double y0 = 0;
  double step = 0.01;
  int maxStepCount = 32;

  QFile file("sine.txt");
  bool b = file.open(QIODevice::WriteOnly);
  QTextStream out(&file);
  for(double i =0; i <= M_PI_2; i+=step)
  {
   sineValue(x0,y0, step , maxStepCount,x0,y0);
   qDebug() << i << " " << x0 << " " << y0;
   out << i << " " << x0 << " " << y0 << '\n';
  }
  file.close();

  return a.exec();
}

